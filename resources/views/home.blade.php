@extends('layouts.app')

@section('content')
<section id="counts" class="counts" style="background: url('{{ asset("assets/img/counts-bg.jpg") }}') center center no-repeat;">
    <div class="container" style="height: 10em"></div>
</section>
<div class="container my-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

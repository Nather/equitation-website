<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-{{ $type }} shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        {{ $title }}</div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $value }}</div>
                </div>
                <div class="col-auto">
                    {!! $logo !!}
                    @if(isset($statValue))
                        <div class="col">
                            <div class="progress progress-sm mr-2">
                                <div class="progress-bar bg-info" role="progressbar"
                                     style="width: {{ $statValue }}%" aria-valuenow="{{ $statValue }}" aria-valuemin="0"
                                     aria-valuemax="100"></div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

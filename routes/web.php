<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::prefix("/admin")->name("admin.")->middleware(["permission:panel"])->group(function () {

    Route::get("/home", [\App\Http\Controllers\Admin\HomeController::class, "index"])->name("index");
    Route::get("/users", [\App\Http\Controllers\Admin\UserController::class, "index"])->name("users.index");
});

Route::get("/debug", [\App\Http\Controllers\DebugController::class, "debug"]);
